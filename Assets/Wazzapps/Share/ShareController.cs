﻿using UnityEngine;
using System.Collections;
using System.IO;
#if UNITY_IOS
using System.Runtime.InteropServices;
#endif

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Wazzapps
{
/*    #if UNITY_EDITOR
    class ShareControllerProcessor : AssetPostprocessor
    {
        private const string DefaultManifest = "<manifest xmlns:android=\"http://schemas.android.com/apk/res/android\" package=\"com.wazzapps.library.sharing\"><application>\n<provider android:name=\"android.support.v4.content.FileProvider\"\nandroid:authorities=\"{0}.provider\"\nandroid:exported=\"false\"\nandroid:grantUriPermissions=\"true\">\n<meta-data android:name=\"android.support.FILE_PROVIDER_PATHS\"\nandroid:resource=\"@xml/file_provider_path\" />\n</provider>\n</application>\n</manifest>";

        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            foreach (string str in importedAssets)
            {
                if(str.Trim().EndsWith("WazzappsShareLibrary/AndroidManifest.xml"))
                {
                    string manifestPath = Application.dataPath + "/Plugins/Android/WazzappsShareLibrary/AndroidManifest.xml";
                    if(File.Exists(manifestPath))
                    {
                        File.Delete(manifestPath);
                    }
                    File.WriteAllText(manifestPath, string.Format(DefaultManifest, Application.identifier));
                    return;
                }
            }
        }
    }
    #endif*/

    public class ShareController : MonoBehaviour
    {
#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void Wazzapps_ShareTextWithImage (string iosPath, string message);
	
	[DllImport("__Internal")]
	private static extern void Wazzapps_ShareText (string message);	
#endif

        IEnumerator ShareCoroutine(string text, string path = null, string type = "image/*")
        {
            yield return new WaitForEndOfFrame();
            ShareNative(text, path, type);
        }

        IEnumerator ShareTextureCoroutine(string text, Texture2D texture)
        {
            yield return new WaitForEndOfFrame();
            string path = SaveTexture(texture);
            ShareNative(text, path);
        }

        private void ShareNative(string text, string path, string type = "image/*")
        {
#if UNITY_ANDROID
            AndroidJavaClass sharerClass = new AndroidJavaClass("com.wazzapps.library.sharing.Sharer");
            if (string.IsNullOrEmpty(path) == false)
            {
                sharerClass.CallStatic("shareTexture", text, path);
            } else {
                sharerClass.CallStatic("share", text);
            }
#elif UNITY_IOS
			if(string.IsNullOrEmpty(path)) {
				Wazzapps_ShareText (text);
			} else {
				Wazzapps_ShareTextWithImage (path, text);
			}
#endif
        }

        private string SaveTexture(Texture2D texture)
        {
            byte[] bytes = texture.EncodeToPNG();
            string path = Application.persistentDataPath + "/s_" + System.DateTime.Now.ToString().Replace(" ", "_").Replace("/", "_") .Replace(":", "_")+ ".png";
            File.WriteAllBytes(path, bytes);
            return path;
        }

        public void Share(string text)
        {
            ShareWithPath(text, null);
        }

        public void ShareWithPath(string text, string path, string type = "image/*")
        {
            if (Application.isEditor)
            {
                Debug.Log(string.Format("ShareWithPath: {0}:{2}, {1}", text, path, type));
            }
            else
            {
                Debug.Log(string.Format("ShareWithPath: {1}, {0}", path, type));
                StartCoroutine(ShareCoroutine(text, path, type));
            }
        }

        public void ShareWithTexture(string text, Texture2D texture)
        {
            if (Application.isEditor)
            {
                Debug.Log(string.Format("ShareWithTexture: {0}", text));
            }
            else
            {
                Debug.Log(string.Format("ShareWithTexture"));
                StartCoroutine(ShareTextureCoroutine(text, texture));
            }
        }

    }
}