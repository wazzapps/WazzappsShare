﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShareExample : MonoBehaviour {
	public Texture2D Texture;

	public Wazzapps.ShareController Sharer;

	public void ShareText()
	{
		Sharer.Share ("Share only text!");
	}

	public void ShareTextAndTexture2D()
	{
		// Texture2d должна быть Read/Write Enabled 
		Sharer.ShareWithTexture ("Share text & texture2d!", Texture);
	}
}
