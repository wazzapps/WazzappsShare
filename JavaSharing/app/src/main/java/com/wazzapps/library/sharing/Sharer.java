package com.wazzapps.library.sharing;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import com.unity3d.player.UnityPlayer;

import java.io.File;

public class Sharer {

	public static void shareTexture(String message, String path) {
		shareTexture(message, path, "image/*");
	}

    public static void shareTexture(String message, String path, String type) {
        Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, message);

		File f = new File(path);
		if(f.exists())
		{
			Uri uri = Uri.fromFile(f);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
				uri = android.support.v4.content.FileProvider.getUriForFile(UnityPlayer.currentActivity,
						UnityPlayer.currentActivity.getPackageName() + ".provider", f);
			}
			sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
		}
		sendIntent.setType(type);
		UnityPlayer.currentActivity.startActivity(sendIntent);
    }

    public static void share(String message) {
        Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, message);
		sendIntent.setType("text/plain");
		UnityPlayer.currentActivity.startActivity(sendIntent);
    }
}
